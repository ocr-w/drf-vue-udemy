from rest_framework.pagination import PageNumberPagination


class MediumSetPagination(PageNumberPagination):
    page_size = 30
