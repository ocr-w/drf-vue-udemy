Vue.component('task-list', {
  props: {
    tasks: {
      type: Array,
      required: true
    },
    counter: {
      type: Number,
      required: true
    }
  },
  data() {
    return {
      new_task: null,
      error: null
    }
  },
  methods: {
    submitTask() {
      if (this.new_task) {
        this.$emit('submit-task', { id: this.counter, content: this.new_task })
        this.new_task = null

        if (this.error) {
          this.error = null
        }
      } else {
        this.error = 'Type here what you want to do :)'
      }
    },
    deleteTask(task) {
      this.$emit('del-todo', task)
    }
  },
  template: `
    <div>
      <form @submit.prevent="submitTask" class="mb-3">
        <div class="form-group">
          <input v-model="new_task" id="taskContent" class="form-control" type="text" placeholder="What do you need to do?">
          <small class="text-muted">{{ this.error }}</small>
        </div>
      </form>
      <task :task="task" :key="task.id" v-for="task in tasks" @del-todo="deleteTask"></task>
    </div>
  `
})

Vue.component('task', {
  props: {
    task: {
      type: Object,
      required: true
    }
  },
  methods: {
    deleteTask(task) {
      this.$emit('del-todo', task)
    }
  },
  template: `
    <div class="alert alert-primary alert-dismissible fade show">
      <span>{{ task.content }}</span>
      <button @click="deleteTask(task)" type="button" class="close" data-dismiss="alert">
        <span>&times;</span>
      </button>
    </div>
  `
})

const app = new Vue({
  el: '#app',
  data: {
    tasks: [],
    counter: 0
  },
  methods: {
    addNewTask(new_task) {
      this.tasks.push(new_task)
    },
    deleteTask(task) {
      this.tasks.splice(this.tasks.indexOf(task), 1)
    }
  },
  computed: {
    remainingTasks() {
      return this.counter = this.tasks.length
    }
  }
})
