const app = new Vue({
  el: '#app',
  data: {
    users: [
      {
        id: 1,
        name: 'alice',
        profession: 'developer'
      },
      {
        id: 2,
        name: 'bob',
        profession: 'builder'
      },
      {
        id: 3,
        name: 'batman',
        profession: 'fighter'
      },
      {
        id: 4,
        name: 'robin',
        profession: 'thief'
      },
      {
        id: 5,
        name: 'spiderman',
        profession: 'swingy boi'
      }
    ]
  }
})
